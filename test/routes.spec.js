import express from 'express';
import routes from '../src/routes.js' 
import { describe, it, before, after } from 'mocha';
import { expect } from 'chai';


const app = express();
const API_URL = 'http://localhost:3000/api/v1'

/** @type {import('node:http').Server} */
let server;
describe("REST API Routes", () => {
    before(() => {
        app.use('/api/v1', routes);
        server = app.listen(3000, () => console.log('Test server started...'));
    });
    it("can get response", async () => {
        const response = await fetch(`${API_URL}/`);
        expect(response.ok).to.be.true; // fast check
    });
    it("should convert HEX TO RGB correctly", async () => {
        const response = await fetch(`${API_URL}/hex-to-rgb?hex=ff8800`);
        const json = await response.json();
        expect(json).to.deep.equal([255, 136, 0]);
    });
    after((done) => {
        server.close(() => done());
    });
});
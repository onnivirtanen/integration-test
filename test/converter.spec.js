import { describe, it } from 'mocha';
import { expect } from 'chai';
import { hex_to_rgb } from '../src/converter.js';

describe("HEX-to-RGB Converter", () => {
    it("is a function", () => {
        expect(hex_to_rgb).to.be.an('function');
    });
    it("should return an array", () => {
        expect(hex_to_rgb("00")).to.be.a('array');
    });
    it("contains three numbers", () => {
        let containsThreeNumbers = true;
        const rgb = hex_to_rgb("#");
        if (rgb.length != 3) containsThreeNumbers = false;
        for (const item in rgb) {
            if (isNaN(item)) {
                containsThreeNumbers = false;
            }
        }
        expect(containsThreeNumbers).to.be.true;
    });
    it("numbers are between 0-255", () => {
        let correctRange = true;
        const rgb = hex_to_rgb("");
        for (const number in rgb) {
            if (!(0 >= number <= 255)) {
                correctRange = false;
            }
        }
        expect(correctRange).to.be.true;
    });
    it("should convert RED value correctly", () => {
        expect(hex_to_rgb("00")).to.deep.equal([0, 0, 0]);
        expect(hex_to_rgb("ff")).to.deep.equal([255, 0, 0]);
        expect(hex_to_rgb("88")).to.deep.equal([136, 0, 0]);
        expect(hex_to_rgb("00")).to.deep.not.equal([136, 0, 0]);
    });
    it("should convert GREEN value correctly", () => {
        expect(hex_to_rgb("00")).to.deep.equal([0, 0, 0]);
        expect(hex_to_rgb("ff")).to.deep.equal([255, 0, 0]);
        expect(hex_to_rgb("88")).to.deep.equal([136, 0, 0]);
        expect(hex_to_rgb("00")).to.not.deep.equal([0, 136, 0]);
    });
    it("should convert BLUE value correctly", () => {
        expect(hex_to_rgb("00")).to.deep.equal([0, 0, 0]);
        expect(hex_to_rgb("ff")).to.deep.equal([255, 0, 0]);
        expect(hex_to_rgb("88")).to.deep.equal([136, 0, 0]);
        expect(hex_to_rgb("00")).to.not.deep.equal([0, 0, 136]);
    });
    it("should convert RGB to HEX correctly", () => {
        expect(hex_to_rgb("000000")).to.deep.equal([0, 0, 0]);
        expect(hex_to_rgb("ffffff")).to.deep.equal([255, 255, 255]);
        expect(hex_to_rgb("ff8800")).to.deep.equal([255, 136, 0]);
        expect(hex_to_rgb("00ff00")).to.not.deep.equal([255, 136, 0]);
    });
});
import { Router } from "express";
import { hex_to_rgb } from '../src/converter.js';

const routes = Router();

routes.get('/', (req, res) => res.status(200).send("Welcome!"));

routes.get('/hex-to-rgb', (req, res) => {
    const rgb = hex_to_rgb(req.query.hex);
    res.status(200).send(rgb);
});

export default routes;
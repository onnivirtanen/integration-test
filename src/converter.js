export const hex_to_rgb = (hex) => {
    // Calculate the number of zeros needed to make the string 6 characters long
    const zerosNeeded = 6 - hex.length;
    // Append the required number of zeros
    hex += '0'.repeat(zerosNeeded);

    // Split the string into an array of three strings, each containing two characters
    const arrayOfHex = [];
    
    arrayOfHex.push(hex.substring(0,2));
    arrayOfHex.push(hex.substring(2,4));
    arrayOfHex.push(hex.substring(4,6));

    // Convert each hex value to an integer, and then to a string
    return arrayOfHex.map(hex => parseInt(hex, 16));
}
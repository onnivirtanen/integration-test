import express from 'express';
import routes from './routes.js' 

const app = express();
app.use(`/api/v1`, routes);
const port = 3000;
const host = "localhost";

app.listen(port, host, () => {
    console.log(`Server: http://localhost:${port}`);
})